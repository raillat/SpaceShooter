﻿using UnityEngine;
using System.Collections;

public class Immune : MonoBehaviour {
    //immune player for 2 sec
    private float delay = 2.0f;
    private float start;
    public static Immune Instance;


    // Use this for initialization
    void Start() {
        start = Time.time;
        Destroy(gameObject.GetComponent<BoxCollider2D>());
    }

    public void reset()
    {
        start = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
	    if(Time.time > start + delay)
        {
            gameObject.AddComponent<BoxCollider2D>();
            Destroy(this);
        }
	}
}
