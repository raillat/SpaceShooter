﻿using UnityEngine;
using System.Collections;

public class detectIfAsteroid : MonoBehaviour {
    private Vector3 rightBottomCameraBorder;
    private Vector3 rightTopCameraBorder;

    // Use this for initialization
    void Start() {
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        //Création d’un tableau contenant tous les asteroids présents dans la scene
        GameObject[] respawns = GameObject.FindGameObjectsWithTag("asteroid");
        //Si le tableau contient au moins un élément, récupération de la taille d’un astéroid (le premier)
        Vector3 siz = new Vector3(0,0,0);
        if (respawns.Length > 0) {
            siz.x = respawns[0].GetComponent<SpriteRenderer>().bounds.size.x;
            siz.y = respawns[0].GetComponent<SpriteRenderer>().bounds.size.y;
        }
        //Si le tableau contient moins de 7 éléments
        if (respawns.Length < 7)
        {
            //Tirage d’une chance de créer ou pas un astéroid
            int rand = Random.Range(1, 100);

            if (rand < 10 && respawns.Length < 4)
            {
                //Création nouvel astéroid avec une position en Y aléatoire 
                Vector3 tmpPos = new Vector3(Random.Range(rightBottomCameraBorder.x + (siz.x / 2), rightBottomCameraBorder.x + (siz.x / 2)*3), Random.Range(rightBottomCameraBorder.y + (siz.y / 2), rightTopCameraBorder.y - (siz.y / 2)), gameObject.transform.position.z);
                //Instanciation de l’astéroid via le préfab
                Instantiate(Resources.Load("asteroidSP"), tmpPos, Quaternion.identity);
            }else if(rand > 95 && respawns.Length < 5)
            {
                Vector3 tmpPos = new Vector3(Random.Range(rightBottomCameraBorder.x + (siz.x / 2), rightBottomCameraBorder.x + (siz.x / 2)*3), Random.Range(rightBottomCameraBorder.y + (siz.y / 2), rightTopCameraBorder.y - (siz.y / 2)), gameObject.transform.position.z);
                Instantiate(Resources.Load("asteroid2"), tmpPos, Quaternion.identity);
            }
        }


    }
}
