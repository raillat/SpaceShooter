﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour {

	// Use this for initialization
	void Start () {
        string [] highscores = GameState.Instance.getBestScores();
        if (highscores != null)
        {
            int cpt = 1;
            foreach (string s in highscores)
            {
                string[] split = s.Split(':');
                GameObject.Find("P" + cpt.ToString()).GetComponent<Text>().text = split[0];
                GameObject.Find("S" + cpt.ToString()).GetComponent<Text>().text = split[1];
                cpt++;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
