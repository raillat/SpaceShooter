﻿using UnityEngine;
using System.Collections;

public class SoundState : MonoBehaviour {
    public static SoundState Instance;
    public AudioClip playerShotSound;
    public AudioClip asteroidShotSound;
    public AudioClip bossShotSound;

    public AudioClip backgroundMusic;
    public AudioClip bossMusic;
    public AudioClip gameOverMusic;
    
    private AudioSource player;
    private float FadeSpeed = 0.35f;
    private FadeState fadeState = FadeState.None;
    private AudioClip next;

    private enum FadeState
    {
        None,
        FadingOut,
        FadingIn
    }

    public AudioSource getPlayer()
    {
        return player;
    }
    // Use this for initialization
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            player = GetComponent<AudioSource>();
        }
        else if (this != Instance)
        {
            Destroy(this.gameObject);
        }
    }

    private void MakeSound(AudioClip originalClip, float volume)
    {
        AudioSource.PlayClipAtPoint(originalClip, transform.position, volume);
    }

    public void touchButtonSound()
    {
        MakeSound(playerShotSound, 1.0f);
    }

    public void asteroidShootSound()
    {
        MakeSound(asteroidShotSound, 0.5f);
    }

    public void bossShootSound()
    {
        MakeSound(bossShotSound, 0.1f);
    }

    public void playNext()
    {
        player.clip = next;
        player.Play();
        this.fadeState = FadeState.FadingIn;
    }

    public void triggerMusic(string music)
    {
        if(music == "boss")
        { 
            next = bossMusic;
        }
        else if(music == "background"){
            next = backgroundMusic;
        }else if(music == "gameover")
        {
            next = gameOverMusic;
        }
        this.fadeState = FadeState.FadingOut;
    }

    public void stopMusic()
    {
        player.Stop();
    }

    // Update is called once per frame
    void Update () {
        if(this.fadeState == FadeState.FadingOut)
        {
            if(this.player.volume > 0.05f)
            {
                this.player.volume -= this.FadeSpeed * Time.deltaTime;
            }
            else
            {
                playNext();
            }
        }else if(this.fadeState == FadeState.FadingIn)
        {
            if(this.player.volume < 50.0f)
            {
                this.player.volume += this.FadeSpeed * Time.deltaTime;
            }
            else
            {
                this.fadeState = FadeState.None;
            }
        }
	}
}
