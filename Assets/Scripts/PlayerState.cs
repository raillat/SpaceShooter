﻿using UnityEngine;
using System.Collections;

public class PlayerState : MonoBehaviour {
    public static PlayerState Instance;
    private int lifePlayer = 5;
    private int dmgPlayer = 1;
    public Vector3 initialLifePos;

    // Use this for initialization
    void Start () {

        if (Instance == null)
        {
            initialLifePos = GameObject.FindGameObjectWithTag("life1").transform.position;
            Instance = this;
        }else if(Instance != this)
        {
            Destroy(this.gameObject);
        }

    }

    public void lifeUp()
    {
        lifePlayer++;
    }

    public void upDmgPlayer(int d, int t)
    {
        dmgPlayer = d;
        //launch coroutine, she reset player's dmg after 5 second
        StartCoroutine(resetDmg(t));
    }

    IEnumerator resetDmg(int t)
    {
        yield return new WaitForSeconds(5);
        dmgPlayer = 1;
    }

    public int getDmgPlayer()
    {
        return dmgPlayer;
    }

    public int getLifePlayer()
    {
        return lifePlayer;
    }

    public void loseLife()
    {
        if (lifePlayer > 0)
        {
            string tag = "life" + lifePlayer.ToString();
            if (GameObject.FindGameObjectWithTag(tag))
            {
                GameObject.FindGameObjectWithTag(tag).AddComponent<fadeOut>();
                GameObject myShip = GameObject.Find("Ship");

                
                if (myShip.GetComponent<makeBlink>())
                {
                    myShip.GetComponent<makeBlink>().reset();
                }
                else
                {
                    myShip.AddComponent<makeBlink>();
                }
            }
        }
        else
        {
            Destroy(GameObject.Find("Ship"));
            Application.LoadLevel("Scene-Scores");
            GameState.Instance.save();
            SoundState.Instance.triggerMusic("gameover");
        }

        lifePlayer--;
        GameObject.Find("Ship").AddComponent<Immune>();
    }

    // Update is called once per frame
    void Update () {
	
	}
}
