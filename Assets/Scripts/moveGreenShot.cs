﻿using UnityEngine;
using System.Collections;

public class moveGreenShot : MonoBehaviour {
    private Vector3 currentPos;
    private float speed = -5.0f;
    private Vector3 coinBasGaucheCamera;
    private Vector3 spriteSize;

    // Use this for initialization
    void Start()
    {
        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
    }


    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Ship")
        {
            PlayerState.Instance.loseLife();
            if (collider.gameObject.GetComponent<Immune>())
            {
                collider.gameObject.GetComponent<Immune>().reset();
            }
            else {
                collider.gameObject.AddComponent<Immune>();
            }
        }
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update () {
        currentPos = gameObject.transform.position;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0.0f);

        if (currentPos.x - spriteSize.x / 2 < coinBasGaucheCamera.x)
        {
            Destroy(gameObject);
        }
    }
}
