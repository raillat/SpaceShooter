﻿using UnityEngine;
using System.Collections;

public class shootBoss : MonoBehaviour {
    private float speed = 8.0f;
    private float fireRate = 2.0f;
    private float nextFire = 0.0f;
    private Vector3 spriteSize;
    private int bullets = 5;
    float yShootSpriteSize;
    float xShootSpriteSize;
    float yShipSpriteSize;

    // Use this for initialization
    void Start () {
        spriteSize = new Vector3(0, 0, 0);
        yShootSpriteSize = (Resources.Load("shootBoss") as GameObject).GetComponent<SpriteRenderer>().bounds.size.y;
        xShootSpriteSize = (Resources.Load("shootBoss") as GameObject).GetComponent<SpriteRenderer>().bounds.size.x;
        yShipSpriteSize = GameObject.Find("Ship").GetComponent<SpriteRenderer>().bounds.size.y;
    }
	
	// Update is called once per frame
	void Update () {
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if(Time.time > nextFire - 1.0f)
        {
            SoundState.Instance.bossShootSound();
        }

        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            //chose a shoot pattern
            if (Random.Range(0,2) == 0)
            {
                // | pattern
                
                Vector3 tmpPos = new Vector3((transform.position.x - spriteSize.x), transform.position.y - (bullets/2)*yShootSpriteSize, 0.0f);

                for (int i = 0; i < bullets; i++)
                {
                    if(i != 0)
                    {
                        tmpPos.y += yShootSpriteSize;
                    }
                    GameObject go = Instantiate(Resources.Load("shootBoss"), tmpPos, Quaternion.identity) as GameObject;
                    go.GetComponent<moveShootBoss>().setSpeed(-speed, 0.0f);
                }
            }
            else
            {
                // > pattern
                Vector3 tmpPos = new Vector3((transform.position.x - spriteSize.x), transform.position.y - yShipSpriteSize/2 - yShootSpriteSize, 0.0f);

                for (int i = 0; i < bullets; i++)
                {
                    if (i != 0)
                    {
                        tmpPos.y -= i * (yShootSpriteSize / 2);
                        tmpPos.x -= i * xShootSpriteSize;
                    }


                    GameObject go = Instantiate(Resources.Load("shootBoss"), tmpPos, Quaternion.identity) as GameObject;
                    go.GetComponent<moveShootBoss>().setSpeed(-speed, 0.0f);
                }

                tmpPos.x = (transform.position.x - spriteSize.x);
                tmpPos.y = (transform.position.y + yShipSpriteSize/2 + yShootSpriteSize);

                for (int i = 0; i < bullets; i++)
                {
                    if (i != 0)
                    {
                        tmpPos.y += i * (yShootSpriteSize / 2);
                        tmpPos.x -= i * xShootSpriteSize;
                    }
                    GameObject go = Instantiate(Resources.Load("shootBoss"), tmpPos, Quaternion.identity) as GameObject;
                    go.GetComponent<moveShootBoss>().setSpeed(-speed, 0.0f);
                }
            }
        }
	}
}
