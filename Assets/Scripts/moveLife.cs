﻿using UnityEngine;
using System.Collections;

public class moveLife : MonoBehaviour {
    private Vector3 coinBasGaucheCamera;
    private Vector3 spriteSize;

	// Use this for initialization
	void Start () {
        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        spriteSize = new Vector2(0, 0);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Ship" || collider.name == "shootOrange(Clone)")
        {
            PlayerState.Instance.lifeUp();
            Vector3 newPos = PlayerState.Instance.initialLifePos;
            newPos.x -= PlayerState.Instance.getLifePlayer()-1;
            GameObject life = Instantiate(Resources.Load("itemLife"),newPos,Quaternion.identity) as GameObject;
            life.tag = "life" + PlayerState.Instance.getLifePlayer().ToString();
            life.transform.parent = GameObject.FindGameObjectWithTag("lifeParent").transform;
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update () {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-5.0f, -1.0f);

        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if (gameObject.transform.position.x - (spriteSize.x / 2) < coinBasGaucheCamera.x || gameObject.transform.position.y - (spriteSize.y / 2) < coinBasGaucheCamera.y)
        {
            Destroy(gameObject);
        }
    }
}
