﻿using UnityEngine;
using System.Collections.Generic;

public class detectIfStars : MonoBehaviour {
    private Vector3 rightBottomCameraBorder;
    private Vector3 rightTopCameraBorder;

    // Use this for initialization
    void Start () {
        rightTopCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {

        //Création d’un tableau contenant tous les asteroids présents dans la scene
        GameObject[] stars = GameObject.FindGameObjectsWithTag("star");
        //Si le tableau contient au moins un élément, récupération de la taille d’un astéroid (le premier)
        Vector3 siz = new Vector3(0, 0, 0);
        if (stars.Length > 0)
        {
            siz.x = stars[0].GetComponent<SpriteRenderer>().bounds.size.x;
            siz.y = stars[0].GetComponent<SpriteRenderer>().bounds.size.y;
        }
        //Si le tableau contient moins de 7 éléments
        if (stars.Length < 7)
        {
            //Tirage d’une chance de créer ou pas un astéroid
            if (Random.Range(1, 100) == 50 || stars.Length < 4)
            {
                //Création nouvel astéroid avec une position en Y aléatoire 
                Vector3 tmpPos = new Vector3(rightBottomCameraBorder.x + (siz.x / 2), Random.Range(rightBottomCameraBorder.y + (siz.y / 2), rightTopCameraBorder.y - (siz.y / 2)), gameObject.transform.position.z);
                //Instanciation de l’astéroid via le préfab
                Instantiate(Resources.Load("Star"), tmpPos, Quaternion.identity);
            }

        }
    }
}
