﻿using UnityEngine;
using System.Collections;

public class shootAsteroid2 : MonoBehaviour {
    private float fireRate = 3.0f;
    private float nextFire = 0.0f;
    private Vector3 spriteSize;

    // Use this for initialization
    void Start () {
        spriteSize = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Vector3 tmpPos = new Vector3((transform.position.x - spriteSize.x), transform.position.y, 0.0f);
            Instantiate(Resources.Load("shootVert"), tmpPos, Quaternion.identity);
            SoundState.Instance.asteroidShootSound();
        }
    }
}
