﻿using UnityEngine;
using System.Collections;

public class moveShootBoss : MonoBehaviour {
    private Vector2 spriteSize;
    private Vector3 coinBasGaucheCamera;
    private Vector3 coinHautGaucheCamera;

    public void setSpeed(float x, float y)
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(x,y);
    }

    // Use this for initialization
    void Start () {
        spriteSize = new Vector2(0, 0);

        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinHautGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Ship")
        {
            for (int i = 0; i < 2; i++)
            {
                PlayerState.Instance.loseLife();
            }
            if (collider.gameObject.GetComponent<Immune>())
            {
                collider.gameObject.GetComponent<Immune>().reset();
            }
            else {
                collider.gameObject.AddComponent<Immune>();
            }
        }
    }

        // Update is called once per frame
        void Update ()
    {
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if (transform.position.x - spriteSize.x/2 < coinBasGaucheCamera.x || transform.position.y - spriteSize.y/2 < coinBasGaucheCamera.y || transform.position.y + spriteSize.y/2 > coinHautGaucheCamera.y)
        {
            Destroy(gameObject);
        }
    }
}
