﻿using UnityEngine;
using System.Collections;

public class shootAgain : MonoBehaviour {
    Vector3 spriteSize;
    private float nextShot = 0.0f;
    private float fireRate = 0.30f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // 
        // Récupération de la taille de l’objet auquel est ataché ce script(le vaisseau)
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
        // Si appui sur la touche espace
        if (!GameState.Instance.isPaused())
        {
            if (((Input.touchCount > 0 && 
                Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0)).x > Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).x / 3
                )|| Input.GetKeyDown(KeyCode.Space)) && Time.time >= nextShot )
            {
                nextShot = Time.time + fireRate;
                SoundState.Instance.touchButtonSound();
                // La position de création du tir se situe à la droite du vaisseau
                Vector3 tmpPos = new Vector3(transform.position.x + spriteSize.x, transform.position.y, transform.position.z);
                //Création d’une instance d’un prefab de typeshootOrange
                if (PlayerState.Instance.getDmgPlayer() == 1)
                {
                    Instantiate(Resources.Load("shootOrange"), tmpPos, Quaternion.identity);
                }
                else
                {
                    Instantiate(Resources.Load("shootPowered"), tmpPos, Quaternion.identity);
                }
            }
        }
    }
}
