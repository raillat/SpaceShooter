﻿using UnityEngine;
using System.Collections;

public class moveBoss : MonoBehaviour {
    public Vector2 spriteSize;
    private Vector3 coinBasGaucheCamera;
    private Vector3 coinHautDroitCamera;
    public int life = 50;
    public int value = 500;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "shoot")
        {
            life-= PlayerState.Instance.getDmgPlayer();
            if (life <= 0)
            {
                GameState.Instance.addScorePlayer(value);
                Destroy(gameObject);
                //re-atach asteroid spawning to Foes
                GameObject.Find("02 - Foes").AddComponent<detectIfAsteroid>();
                GameObject.Find("02 - Foes").GetComponent<detectIfBoss>().setSpawnable();
                SoundState.Instance.triggerMusic("background");
            }
        }
    }


    // Use this for initialization
    void Start () {
        spriteSize = new Vector2(0, 0);
        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinHautDroitCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }
	
	// Update is called once per frame
	void Update () {
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //boss come to right camera border
        if (gameObject.transform.position.x + (spriteSize.x/2) > coinHautDroitCamera.x) {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-2.0f, 0.0f);
        }
        else
        {
            if (transform.position.x > (coinHautDroitCamera.x - (coinHautDroitCamera.x/4)) && transform.position.y + spriteSize.y/2 < coinHautDroitCamera.y)
            {
                //| |O|
                //| | |
                //right camera border
                //chose between going up or going down
                //diagonal up/left
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-2.0f,2.0f);
                //Debug.Log("cas 1");
                
            } else if (transform.position.x <= (coinHautDroitCamera.x - (coinHautDroitCamera.x / 4)) && transform.position.x > coinHautDroitCamera.x/2 && transform.position.y >= coinHautDroitCamera.y /2)
            {
                // |O| |
                // | | |
                //top camera border
                //chose between going left or right

                //diagonal bottom/left
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-2.0f, -2.0f);
                //Debug.Log("cas 2");

            }
            else if (transform.position.x <= coinHautDroitCamera.x/2 && transform.position.y < coinHautDroitCamera.y /2 && transform.position.y >= coinBasGaucheCamera.y )
            {
                //| | |
                //|O| |
                //middle screen
                //chose between going up or down
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(2.0f, -2.0f);

                //Debug.Log("cas 3");
            }
            else if(transform.position.x > coinHautDroitCamera.x - (coinHautDroitCamera.x / 4) && transform.position.x + spriteSize.x/2 < coinHautDroitCamera.x && transform.position.y < coinHautDroitCamera.y / 2)
            {
                //| | |
                //| |O|
                //bottom camera border
                //chose between left or right
                gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(2.0f, 2.0f);
                //Debug.Log("cas 4");
            }
        }
    }
}
