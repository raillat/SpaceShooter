﻿using UnityEngine;
using System.Collections;

public class moveStar : MonoBehaviour {
    private Vector3 coinBasGaucheCamera;
    private Vector3 coinHautDroitCamera;
    private Vector3 spriteSize;
    public float speed = 5.0f;

	// Use this for initialization
	void Start () {
        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinHautDroitCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }
	
	// Update is called once per frame
	void Update () {

        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0.0f);

        //star is out of the screen
        if (gameObject.transform.position.x - (spriteSize.x / 2) < coinBasGaucheCamera.x)
        {
            Vector3 newPos = new Vector3(coinHautDroitCamera.x, Random.Range(coinBasGaucheCamera.y, coinHautDroitCamera.y), gameObject.transform.position.z);
            Instantiate(Resources.Load("Star"), newPos, Quaternion.identity);
            Destroy(gameObject);
        }

    }
}
