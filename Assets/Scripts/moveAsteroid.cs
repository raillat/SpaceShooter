﻿using UnityEngine;
using System.Collections;

public class moveAsteroid : MonoBehaviour {
    private Vector3 currentPos;
    private Vector3 coinHautGaucheCamera;
    private Vector3 coinBasDroiteCamera;
    private Vector3 coinBasGaucheCamera;
    private Vector3 spriteSize;
    public int life;
    public int value;
    // Use this for initialization
    void Start () {
        //init pos
        coinHautGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinBasDroiteCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        currentPos = new Vector3(coinBasDroiteCamera.x,Random.Range(coinBasGaucheCamera.y,coinHautGaucheCamera.y), 0);
        gameObject.transform.position = currentPos;
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.name == "Ship")
        {
            PlayerState.Instance.loseLife();



            if (collider.gameObject.GetComponent<Immune>())
            {
                collider.gameObject.GetComponent<Immune>().reset();
            }
            else {
                collider.gameObject.AddComponent<Immune>();
            }
        }
        else if (collider.tag == "asteroid")
        {
            Destroy(collider.gameObject);
        }
    }

    public void setLife(int l)
    {
        life = l;
    }

    // Update is called once per frame
    void Update () {
        currentPos = transform.position;
        //get sprite size
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //set asteroid's speed
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(GameState.Instance.getSpeed(), 0.0f);
        
        if (gameObject.transform.position.x - (spriteSize.x/2) < coinHautGaucheCamera.x)
        {
            Vector3 newPos = new Vector3(coinBasDroiteCamera.x, Random.Range(coinBasGaucheCamera.y + (spriteSize.y/2), coinHautGaucheCamera.y - (spriteSize.y/2)), gameObject.transform.position.z);
            Instantiate(Resources.Load("asteroidSP"), newPos, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {

    }
}
