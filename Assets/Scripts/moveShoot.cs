﻿using UnityEngine;
using System.Collections;

public class moveShoot : MonoBehaviour {
    private Vector3 currentPos;
    private float speed = 5.0f;
    private Vector3 coinBasDroitCamera;
    private Vector3 spriteSize;

    // Use this for initialization
    void Start () {
        coinBasDroitCamera = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "asteroid")
        {
            collider.gameObject.GetComponent<moveAsteroid>().life-=PlayerState.Instance.getDmgPlayer();
            if (collider.gameObject.GetComponent<moveAsteroid>().life <= 0)
            {
                collider.gameObject.AddComponent<fadeOut>();
                GameState.Instance.addScorePlayer(collider.gameObject.GetComponent<moveAsteroid>().value);

                int rand = Random.Range(0, 101);
                Vector3 pos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
                if (PlayerState.Instance.getLifePlayer() < 8)
                {
                    //10% chance getting a life up (if max life is not reach)
                    if (rand < 10)
                    {
                        Instantiate(Resources.Load("lifeUp"), pos, Quaternion.identity);
                    }
                }

                if (rand > 85)
                {
                    //15% chance getting dmg up
                    Instantiate(Resources.Load("shootUp"), pos, Quaternion.identity);
                }

                Destroy(collider);
            }
        }
        Destroy(gameObject);
    }

        // Update is called once per frame
    void Update () {
        currentPos = gameObject.transform.position;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0.0f);

        if (currentPos.x + spriteSize.x/2 > coinBasDroitCamera.x)
        {
            Destroy(gameObject);
        }
    }
}
