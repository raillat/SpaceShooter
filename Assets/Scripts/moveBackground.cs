﻿using UnityEngine;
using System.Collections;

public class moveBackground : MonoBehaviour {
    private Vector3 spriteSize;
    private float positionRestartX;
    private Vector3 leftBottomCameraBorder;

    public void setStartX(float posX)
    {
        positionRestartX = posX;
    }

	// Use this for initialization
	void Start () {
        spriteSize = new Vector3(0, 0, 0);
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Rigidbody2D>().velocity = new Vector2(-5.0f,0.0f);
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        if(transform.position.x < leftBottomCameraBorder.x - (spriteSize.x / 2))
        {
            transform.position = new Vector3(positionRestartX, transform.position.y, transform.position.z);
        }
    }
}
