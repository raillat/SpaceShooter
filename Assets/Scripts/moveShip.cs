﻿using UnityEngine;
using System.Collections;

public class moveShip : MonoBehaviour {
    public float speed = 8.0F;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        //get up/down input
        float movmentup;
        
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, 0)).x < Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0)).x / 3)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            if (touchDeltaPosition.y > 0)
            {
                movmentup = 1.5f;
            }
            else {
                movmentup = -1.5f;
            }
        }
        else
        {
            movmentup = Input.GetAxis("Vertical");
        }
        

        //move ship up or down with speed value
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, movmentup * speed);
    }
}
