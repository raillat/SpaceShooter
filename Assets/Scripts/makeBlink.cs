﻿using UnityEngine;
using System.Collections;

public class makeBlink : MonoBehaviour {
    private float delay = 2.0f;
    private float nextTime;
    private float init;
    private bool isEnabled = true;

	// Use this for initialization
	void Start () {
        init = Time.time;

    }

    public void reset()
    {
        init = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.time < init + delay)
        {
            if (Time.time > nextTime)
            {
                nextTime = Time.time + 0.1f;
                isEnabled = !isEnabled;
                gameObject.GetComponent<SpriteRenderer>().enabled = isEnabled;
            }
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = true;
            Destroy(this);
        }
	}
}
