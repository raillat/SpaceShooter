﻿using UnityEngine;
using System.Collections;

public class posShip : MonoBehaviour {
    private Vector3 coinHautGaucheCamera;
    private Vector3 coinBasGaucheCamera;
    private Vector3 spriteSize;

    // Use this for initialization
    void Start ()
    {
        coinHautGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        coinBasGaucheCamera = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        spriteSize.x = gameObject.GetComponent<SpriteRenderer>().bounds.size.x;
        spriteSize.y = gameObject.GetComponent<SpriteRenderer>().bounds.size.y;

        //set ship at middle left of the screen
        transform.position = new Vector3(coinHautGaucheCamera.x + spriteSize.x/2, 0, transform.position.z);

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = gameObject.transform.position;
        if ((pos.y + (spriteSize.y / 2)) > coinHautGaucheCamera.y)
        {
            gameObject.transform.position = new Vector3(pos.x, coinHautGaucheCamera.y - (spriteSize.y/2),pos.z);
        }
        else if ((pos.y - (spriteSize.y / 2)) < coinBasGaucheCamera.y)
        {
            gameObject.transform.position = new Vector3(pos.x, coinBasGaucheCamera.y + (spriteSize.y/2), pos.z);
        }
    }
}
