﻿using UnityEngine;
using System.Collections;

public class init : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //position lives
        Vector3 cameraBottomRight = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        float posX = 0.0f;
        for(int i = 1; i<6; i++)
        {
            SpriteRenderer life = GameObject.FindGameObjectWithTag("life"+i.ToString()).GetComponent<SpriteRenderer>();
            if (i == 1)
            {
                posX = cameraBottomRight.x - life.sprite.bounds.size.x / 2;
            }
            else
            {
                posX -= life.sprite.bounds.size.x;
            }

            if (life != null)
            {
                GameObject.FindGameObjectWithTag("life" + i.ToString()).transform.position = new Vector3(posX, cameraBottomRight.y + life.sprite.bounds.size.y / 2, 0);
            }
        }

        SpriteRenderer bg1 = GameObject.FindGameObjectWithTag("bg1").GetComponent<SpriteRenderer>();
        SpriteRenderer bg2 = GameObject.FindGameObjectWithTag("bg2").GetComponent<SpriteRenderer>();
        SpriteRenderer bg3 = GameObject.FindGameObjectWithTag("bg3").GetComponent<SpriteRenderer>();


        //resize backgrounds
        /*float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;*/
        Vector3 coinHautDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
        Vector3 coinBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));

        float scaley = (coinHautDroit.y + coinBasGauche.y * -1) / bg1.bounds.size.y;
        float scalex = (coinHautDroit.x + coinBasGauche.x * -1) / bg1.bounds.size.x;

        Vector3 final = new Vector3(scalex, scaley, 1);
        
        bg1.transform.localScale = final;
        bg2.transform.localScale = final;
        bg3.transform.localScale = final;

        Vector2 size = GameObject.FindGameObjectWithTag("bg1").GetComponent<SpriteRenderer>().bounds.size;

        //positioning backgrounds
        bg1.transform.position = new Vector3(coinBasGauche.x + size.x /2, 0, 1);
        bg2.transform.position = new Vector3(coinBasGauche.x + size.x * 1.5f, bg1.transform.position.y, bg1.transform.position.z);
        bg3.transform.position = new Vector3(coinBasGauche.x + size.x * 2.5f, bg2.transform.position.y, bg2.transform.position.z);

        float positionX = GameObject.FindGameObjectWithTag("bg3").GetComponent<SpriteRenderer>().transform.position.x;

        bg1.GetComponent<moveBackground>().setStartX(positionX);
        bg2.GetComponent<moveBackground>().setStartX(positionX);
        bg3.GetComponent<moveBackground>().setStartX(positionX);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
