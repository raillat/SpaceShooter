﻿using UnityEngine;
using System.Collections;

public class detectIfBoss : MonoBehaviour {
    private Vector3 rightBottomCameraBorder;
    private bool spawnable = true;
    private int nextSpawn = 200;

    public void setSpawnable()
    {
        spawnable = true;
    }

	// Use this for initialization
	void Start () {
	    rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }
	
	// Update is called once per frame
	void Update () {
        //every 200 points, spawn a boss
        if (spawnable && GameState.Instance.getScorePlayer() >= nextSpawn)
        {
            Vector3 tmpPos = new Vector3(rightBottomCameraBorder.x, rightBottomCameraBorder.y, 0.0f);
            Instantiate(Resources.Load("boss"), tmpPos, Quaternion.identity);
            SoundState.Instance.triggerMusic("boss");
            Destroy(gameObject.GetComponent<detectIfAsteroid>());
            foreach (GameObject  g in GameObject.FindGameObjectsWithTag("asteroid"))
            {
                Destroy(g);
            }
            //Destroy(gameObject);
            nextSpawn += 200 + 500;
            spawnable = false;
        }
	}
}
